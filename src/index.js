'use strict';
const express = require('express');

const middlewares = require('./middlewares/middlewarse.js')
const video = require('./routes/video.js');
// Constants
const PORT = 8080;
const HOST = '127.0.0.1';

// App
const app = express();

app.use(middlewares.urlencoded)
app.use(middlewares.json)
app.use(middlewares.cors)

video(app)

app.set('json spaces', 3);

const server = app.listen(PORT);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports = server;
