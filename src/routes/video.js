const express = require('express');

const queues = require('../queues/video.js')
const router = express.Router();

const app = router => {
    router.get('/monkey', queues.getMonkey)
    router.get('/', queues.init)
}

module.exports = app;