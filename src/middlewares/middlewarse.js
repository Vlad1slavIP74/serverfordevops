const bodyParser = require('body-parser');
const cors = require('cors');

module.exports = {
    json: bodyParser.json(),
    urlencoded: bodyParser.urlencoded({
        extended: true
    }),
    cors: cors()
}