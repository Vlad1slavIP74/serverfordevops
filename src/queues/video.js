const fs = require('fs')

const getMonkey = async(req, res) => {
    const path = 'src/video/video(11).mp4'
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range
    console.log('range', range)
    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1] 
          ? parseInt(parts[1], 10)
          : fileSize-1
        const chunksize = (end-start)+1
        const file = fs.createReadStream(path, {start, end})
        const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': 'video/mp4',
        }
        res.writeHead(206, head);
        file.pipe(res);
      } else {
        const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
      }
    
}

const init = (req,res) => {
    return res.status(200).json({msg: 'Hi'});
}

module.exports = {
    getMonkey,
    init
}